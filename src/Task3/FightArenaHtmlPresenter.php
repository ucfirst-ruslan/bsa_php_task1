<?php

declare(strict_types=1);

namespace App\Task3;

use App\Task1\FightArena;

class FightArenaHtmlPresenter
{
	/**
	 * @param FightArena $arena
	 * @return string
	 */
    public function present(FightArena $arena): string
    {
		$arenaResult = null;

		foreach ($arena->all() as $fighter) {
			$arenaResult .= '<div><img src="'.$fighter->image.'">' . $fighter->name .': '. $fighter->attack.', '. $fighter->health . '</div>';
		}

		return $arenaResult;
    }
}
