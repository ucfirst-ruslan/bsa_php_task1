<?php

declare(strict_types=1);

namespace App\Task1;

class FightArena
{

	/**
	 * FightArena
	 * @var array
	 */
	public $arena = [];


	/**
	 * Add new fighter
	 *
	 * @param \App\Task1\Fighter $fighter
	 */
	public function add(Fighter $fighter): void
    {
	    $this->arena[] = $fighter;
    }

	/**
	 * Get all fighters
	 *
	 * @return array
	 */
	public function all(): array
	{
		return $this->arena;
	}

	/**
	 * Sort fighters by attack
	 *
	 * @return \App\Task1\Fighter
	 */
    public function mostPowerful(): Fighter
    {
		usort($this->arena, function($a, $b) {
			return ($b->attack <=> $a->attack);
		});

		return current($this->arena);
    }

	/**
	 * Sort fighters by health
	 *
	 * @return \App\Task1\Fighter
	 */
    public function mostHealthy(): Fighter
    {
		usort($this->arena, function($a, $b) {
			return ($b->health <=> $a->health);
		});

		return current($this->arena);
    }
}
