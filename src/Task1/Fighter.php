<?php

declare(strict_types=1);

namespace App\Task1;

/**
 * Class Fighter
 *
 * @package App\Task1
 */
class Fighter
{
	/**
	 * id fighter
	 * @var int
	 */
	public $id;

	/**
	 * name fighter
	 * @var string
	 */
	public $name;

	/**
	 * health fighter
	 * @var int
	 */
	public $health;

	/**
	 * attack fighter
	 * @var int
	 */
	public $attack;

	/**
	 * image fighter
	 * @var string
	 */
	public $image;


	/**
	 * Fighter constructor.
	 *
	 * @param int $id
	 * @param string $name
	 * @param int $health
	 * @param int $attack
	 * @param string $image
	 */
	public function __construct(int $id, string $name, int $health, int $attack, string $image)
	{
		$this->id = $id;
		$this->name = $name;
		$this->health = $health;
		$this->attack = $attack;
		$this->image = $image;
	}

	/**
	 * Get id
	 * @return int
	 */
    public function getId(): int
    {
        return $this->id;
    }

	/**
	 * Get name
	 * @return string
	 */
    public function getName(): string
    {
        return $this->name;
    }

	/**
	 * Get health
	 * @return int
	 */
    public function getHealth(): int
    {
        return $this->health;
    }

	/**
	 * Get attack
	 * @return int
	 */
    public function getAttack(): int
    {
        return $this->attack;
    }

	/**
	 * Get image
	 * @return string
	 */
    public function getImage(): string
    {
        return $this->image;
    }
}
