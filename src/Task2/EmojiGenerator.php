<?php

declare(strict_types=1);

namespace App\Task2;

/**
 * Class EmojiGenerator
 *
 * @package App\Task2
 */
class EmojiGenerator
{
	/**
	 * @var array
	 */
	protected $emojis;

	/**
	 * EmojiGenerator constructor.
	 */
	public function __construct()
	{
		$this->emojis = ['🚀', '🚃', '🚄', '🚅', '🚇'];
	}

	/**
	 * @return \Generator
	 */
	public function generate(): \Generator
    {
    	foreach ($this->emojis as $emoji) {
		    yield $emoji;
	    }
    }
}
